package org.example.task4_wiseMan.philosopher;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.Semaphore;

public class Main {
    List<Thread> threads = new ArrayList<>();
    List<Philosopher> philosophers = new LinkedList<>();
    Semaphore semaphore = new Semaphore(5);

    public Main() {
        init();
    }

    private void init() {
        for (int i = 0; i < 5; i++) {
            Philosopher philosopher = new Philosopher(semaphore, this);
            Thread thread = new Thread(philosopher);

            philosophers.add(i, philosopher);
            thread.start();
            threads.add(thread);
        }

        for (Thread thread : threads) {
            try {
                thread.join();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    public static void main(String[] args) {
        new Main();
    }
}
