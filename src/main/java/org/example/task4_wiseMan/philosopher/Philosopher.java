package org.example.task4_wiseMan.philosopher;

import org.example.MyOwnLib;

import java.util.concurrent.Semaphore;

public class Philosopher implements Runnable {
    private int forks = 0; // available values: 0, 1, 2
    private final Semaphore semaphore;
    private final Main main;

    public Philosopher(Semaphore semaphore, Main main) {
        this.semaphore = semaphore;
        this.main = main;
    }

    public int getForks() {
        return forks;
    }

    public void setForks(int forks) {
        if (forks >= 0 && forks < 3) {
            this.forks = forks;
        } else {
            System.out.println("out of forks exeption ((");
        }
    }

    public void thinking() {
        pausing();
        System.out.println("Philosopher is thinking;");
    }

    public void takingForks() {
        synchronized (main) {
            try {
                semaphore.acquire();
                semaphore.acquire();
                forks += 2;
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    public void eating() {
        pausing();
        System.out.println("Philosopher eating..");
        forks = 0;
        semaphore.release(2);
    }

    private void pausing() {
        //MyOwnLib.timing(MyOwnLib.randomInt2(1, 10000));
        try {
            Thread.sleep(MyOwnLib.randomInt2(1, 100));
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void run() {
        while (true) {
            if (forks == 0) {
                thinking();
                takingForks();
            } else if (forks == 2) {
                eating();
            } else System.out.println("что-то пошло не так..");
        }
    }
}

//объеденить философов и майранабл в один класс