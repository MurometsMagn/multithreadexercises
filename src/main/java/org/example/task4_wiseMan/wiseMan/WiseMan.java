package org.example.task4_wiseMan.wiseMan;
//sage, wiseacre, mage, sapient, sadhu, swami - мудрецы
/*
 Пять философов сидят за столом и занимаются размышлениями.
 Когда они проголодаются, они должны взять две вилки и начать есть
 (считается, что количество еды доступно неограниченное).
 Вилок у философов всего пять и они не могут начать есть,
 пока не возьмут обе требуемые вилки. В случае, если недостаточно свободных вилок,
 философ берет сколько может и продолжает размышления, пока не освободятся еще вилки.
 После того, как обе вилки взяты, он питается в течение 1 секунды (функция sleep) и кладет вилки обратно.
 Требуется реализовать описанную систему.

 В отличие от классической задачи об обедающих философах, здесь нет принадлежности вилки определенному философу.
 Вместо этого любой философ может взять любую незанятую вилку.
 Так, для представления вилок в программе необходимо использовать семафор на 5 позиций (5 вилок).

 Из-за того, что философы должны захватывать две вилки и это невозможно сделать атомарно,
 может возникнуть ситуация, когда каждый из пяти философов взял по одной вилке.
 Так как они не могут положить взятую вилку обратно, данная ситуация является ситуацией тупика (deadlock).
 Необходимо предусмотреть решение этой проблемы.
 */

import org.example.MyOwnLib;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Semaphore;

public class WiseMan {
    Semaphore fork_semaphore = new Semaphore(5); // 5 wiseMen
    int[] wisemanHasForks = new int[5]; // available values: 0, 1, 2
    List<Thread> threads = new ArrayList<>();

    public WiseMan() {
        for (int i = 0; i < wisemanHasForks.length; i++) {
            Runnable runnable = new MyRunnable(i, this);
            Thread thread = new Thread(runnable);
            thread.start();
            threads.add(thread);
        }

        for (Thread thread : threads) {
            try {
                thread.join();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    public void thinking(int wiseManNumber) {
        MyOwnLib.timing(MyOwnLib.randomInt2(1, 10000));
        System.out.println("wiseMan nmb " + wiseManNumber + " is thinking;");
    }

    public boolean tryTakeFork(int wiseManNumber) {
        boolean success = false;
        if (success = fork_semaphore.tryAcquire()) {
            wisemanHasForks[wiseManNumber]++;
        }
        return success;
    }

    public void eating(int wiseManNumber) {
        MyOwnLib.timing(MyOwnLib.randomInt2(1, 10000));
        System.out.println("wiseMan nmb " + wiseManNumber + " is eating;");
        wisemanHasForks[wiseManNumber] = 0;
        fork_semaphore.release(2);
    }

    class MyRunnable implements Runnable {
        private final int wiseManNumber;
        //private int[] wisemanHasForks;
        private final WiseMan wiseMan;

        public MyRunnable(int wiseManNumber, WiseMan wiseMan) {
            this.wiseManNumber = wiseManNumber;
            this.wiseMan = wiseMan;
        }

        @Override
        public void run() {
            while (true) {
                switch (wisemanHasForks[wiseManNumber]) {
                    case 0 -> {
                        wiseMan.thinking(wiseManNumber);
                        wiseMan.tryTakeFork(wiseManNumber);
                    }
                    case 1 -> {
                        if (!wiseMan.tryTakeFork(wiseManNumber)) {
                            wisemanHasForks[wiseManNumber]--; //drop a fork //здесь должна быть ошибка
                            fork_semaphore.release();
                        }
                    }
                    case 2 -> wiseMan.eating(wiseManNumber);
                    default -> System.out.println("что- то пошло не так..");
                }
            }
        }
    }


}

class Main {
    public static void main(String[] args) {
        new WiseMan();
    }
}