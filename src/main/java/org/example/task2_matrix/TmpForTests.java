package org.example.task2_matrix;

public class TmpForTests {
    public int[][] singleThreaded(int[][] firstMatrix, int[][] secondMatrix) {
        int a = firstMatrix.length; //количество рядов
        int b = firstMatrix[0].length; //количество столбцов
        int d = secondMatrix[0].length; //количество столбцов
        int[][] resultMatrix = new int[a][d];
        for (int i = 0; i < a; i++) {
            for (int j = 0; j < d; j++) {
                for (int k = 0; k < b; k++) {
                    resultMatrix[i][j] += firstMatrix[i][k] * secondMatrix[k][j];
                }
            }
        }
        //System.out.println(Arrays.deepToString(resultMatrix));
        return resultMatrix;
    }
}
