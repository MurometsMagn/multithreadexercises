package org.example.task2_matrix;

import java.util.InputMismatchException;
import java.util.Queue;
import java.util.Scanner;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class MainWithExecutorService {
    private static final Scanner scanner = new Scanner(System.in);
    private static int rows1, cols1, rows2, cols2;
    private static int[][] firstMatrix;
    private static int[][] secondMatrix;
    private static int nThreads;

    public static void main(String[] args) {
        matrixTask();
        ExecutorService executorService = Executors.newFixedThreadPool(nThreads);
        //final Queue<Integer> numbers = new ConcurrentLinkedQueue<>();

        //localSubmiter();
        int[][] resultMatrix = new int[rows1][cols2];
        for (int i = 0; i < rows1; i++) {
            for (int j = 0; j < cols2; j++) {
                //myThreadPool.addTask(new MainWithMyThreadPool.InternalCounterData(firstMatrix, secondMatrix, resultMatrix, i, j));
                executorService.submit(new Runner(new InternalCounterData(firstMatrix, secondMatrix, resultMatrix, i, j)));
            }
        }

        executorService.shutdown();
        soutMatrix(firstMatrix);
        soutMatrix(secondMatrix);
        soutMatrix(resultMatrix);
    }

    private static void matrixTask() {
        requestMatrixSizes();
        System.out.println("Введите количество потоков n: ");
        nThreads = inputInteger();
        firstMatrix = initMatrixRandom(rows1, cols1);
        secondMatrix = initMatrixRandom(rows2, cols2);
    }

    private static int inputInteger() {
        int n;
        while (true) {
            try {
                n = scanner.nextInt();
                if (n > 0) return n;
                else System.out.println("некорректное значение, попробуйте еще раз");
            } catch (InputMismatchException ex) {
                System.out.println("Try again.  - Incorrect input: an integer is required)");
            }
        }
    }

    private static void soutMatrix(int[][] matrix) {
        System.out.println();
        for (int i = 0; i < matrix.length; i++) {
            for (int j = 0; j < matrix[0].length; j++) {
                System.out.print(" " + matrix[i][j] + " ");
            }
            System.out.println();
        }
    }

    private static void requestMatrixSizes() {
        while (true) {
            System.out.println("Введите число строк первой матрицы");
            rows1 = inputInteger();

            System.out.println("Введите число столбцов первой матрицы");
            cols1 = inputInteger();

            System.out.println("Введите число строк второй матрицы");
            rows2 = inputInteger();

            System.out.println("Введите число столбцов второй матрицы");
            cols2 = inputInteger();

            if (cols1 == rows2)
                break;
            else
                System.out.println("Матрицы несогласованы, тобишь неперемножаемы, попробуйте еще раз.");
        }
    }

    private static int[][] initMatrixRandom(int y, int x) {
        int[][] matrix = new int[y][x];
        for (int i = 0; i < y; i++) {
            for (int j = 0; j < x; j++) {
                matrix[i][j] = (int) (Math.random() * 10);
            }
        }
        //System.out.println(Arrays.deepToString(matrix));
        return matrix;
    }

    private static final class InternalCounterData {

        int[][] firstMatrix;
        int[][] secondMatrix;
        int[][] result;
        int row,  col;
        public InternalCounterData(int[][] firstMatrix, int[][] secondMatrix, int[][] resultMatrix, int row, int col) {
            this.firstMatrix = firstMatrix;
            this.secondMatrix = secondMatrix;
            this.result = resultMatrix;
            this.row = row;
            this.col = col;
        }

    }
    private static final class Runner implements Runnable {
        InternalCounterData data;

        public Runner(InternalCounterData data) {
            this.data = data;
        }

        @Override
        public void run() {
            internalCounter(data);
        }

        private static void internalCounter(InternalCounterData data) {
            int b = data.firstMatrix[0].length;
            int result = 0;
            for (int k = 0; k < b; k++) {
                result += data.firstMatrix[data.row][k] * data.secondMatrix[k][data.col];
            }
            data.result[data.row][data.col] = result;
        }
    }
}
