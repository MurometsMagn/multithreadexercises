package org.example.task2_matrix;

import java.time.Duration;
import java.time.Instant;
import java.util.*;

/*
 Пользователь вводит размеры двух матриц (количество строк и столбцов в них).
 Содержимое матриц генерируется случайным образом.
 Если матрицы указанного размера перемножить нельзя, вывести сообщение об ошибке и завершить программу.
 Остальная часть задачи описывает ситуацию, когда матрицы указанного размера перемножить можно.

Необходимо реализовать две версии перемножения этих матриц: последовательное и параллельное.
Оба эти варианта реализовать как отдельные функции.
Замерить скорость выполнения обоих вариантов.
При замере скорости выполнения параллельного варианта также учитывать время создания потоков и ожидание их завершения.
Печать результата перемножения ни в последовательном, ни в параллельном варианте не учитывать в подсчете времени.

Сравнить время последовательного и параллельного вычисления для различных размеров матриц.
Объяснить полученные результаты.

Примечание 1:
так как при подсчете произведения матриц не требуется разным потокам обращаться к одинаковым ячейкам,
явной синхронизации этих потоков не нужно; таким образом перемножение матриц это идеально распараллеливаемая задача.

Примечание 2:
если вы не помните или не знаете как перемножаются матрицы, ознакомьтесь с формулой и алгоритмом тут:
https://ru.wikipedia.org/wiki/%D0%A3%D0%BC%D0%BD%D0%BE%D0%B6%D0%B5%D0%BD%D0%B8%D0%B5_%D0%BC%D0%B0%D1%82%D1%80%D0%B8%D1%86
 */
public class Main {
    static Scanner scanner = new Scanner(System.in);
    static int a, b, c, d;

    public static void main(String[] args) {
        try {


            requestMatrixSizes();
//            a = 2; //строк
//            b = c = 2;
//            d = 3; //столбцов

            //int[][] firstMatrix = new int[a][b];
            //int[][] secondMatrix = new int[c][d];
            int[][] resultMatrix = new int[a][d];
            int[][] resultMatrix2 = new int[a][d];

            int[][] firstMatrix = initMatrixRandom(a, b);
            int[][] secondMatrix = initMatrixRandom(c, d);
//            int[][] firstMatrix = {{1, 2}, {3, 4}};
//            int[][] secondMatrix = {{1, 2, 3}, {4, 5, 6}};
            //int[][] resultMatrix = {{9, 12, 15}, {19, 26, 33}};

            //Instant and Duration from java.time.api
            Instant start = Instant.now();
            resultMatrix = singleThreaded(firstMatrix, secondMatrix);
            Instant finish = Instant.now();
            long elapsed = Duration.between(start, finish).toMillis();
            System.out.println("Однопоточное вычисление, мс: " + elapsed);
            //soutMatrix(resultMatrix);

            long start2 = System.currentTimeMillis(); //либо System.nanoTime();
            resultMatrix2 = multithreadCount(firstMatrix, secondMatrix);
            long finish2 = System.currentTimeMillis();
            long elapsed2 = finish2 - start2;
            System.out.println("Многопоточное вычисление, мс: " + elapsed2);

//            soutMatrix(firstMatrix);
//            soutMatrix(secondMatrix);
//            soutMatrix(resultMatrix2);

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private static void soutMatrix(int[][] matrix) {
        System.out.println();
        for (int i = 0; i < matrix.length; i++) {
            for (int j = 0; j < matrix[0].length; j++) {
                System.out.print(" " + matrix[i][j] + " ");
            }
            System.out.println();
        }
    }

    private static void requestMatrixSizes() {
        while (true) {
            System.out.println("Введите число строк первой матрицы");
            a = inputInteger();

            System.out.println("Введите число столбцов первой матрицы");
            b = inputInteger();

            System.out.println("Введите число строк второй матрицы");
            c = inputInteger();

            System.out.println("Введите число столбцов второй матрицы");
            d = inputInteger();

            if (b == c)
                break;
            else
                System.out.println("Матрицы несогласованы, тобишь неперемножаемы, попробуйте еще раз.");
        }
    }

    private static int[][] initMatrixRandom(int y, int x) {
        int[][] matrix = new int[y][x];
        for (int i = 0; i < y; i++) {
            for (int j = 0; j < x; j++) {
                matrix[i][j] = (int) (Math.random() * 10);
            }
        }
        //System.out.println(Arrays.deepToString(matrix));
        return matrix;
    }

    public static int[][] singleThreaded(int[][] firstMatrix, int[][] secondMatrix) {
        int a = firstMatrix.length;
        int b = firstMatrix[0].length;
        int d = secondMatrix[0].length;

        int[][] resultMatrix = new int[a][d];
        for (int i = 0; i < a; i++) {
            for (int j = 0; j < d; j++) {
//                for (int k = 0; k < b; k++) {
//                    resultMatrix[i][j] += firstMatrix[i][k] * secondMatrix[k][j];
//                }
                resultMatrix[i][j] = internalCounter(firstMatrix, secondMatrix, i, j);
            }
        }
        //System.out.println(Arrays.deepToString(resultMatrix));
        return resultMatrix;
    }

    public static int[][] multithreadCount(int[][] firstMatrix, int[][] secondMatrix) {
        int a = firstMatrix.length;
        int b = firstMatrix[0].length;
        int d = secondMatrix[0].length;
        int[][] resultMatrix = new int[a][d];

        System.out.println("Введите количество потоков n: ");
        int n = inputInteger();

        int numbers = a * d; //количество ячеек результирующей матрицы
        if (n > numbers) n = numbers; //если потоков больше, чем вычислений
        int numbersPerThread = numbers / n;
        int numbersPerMainThread = numbers % n;
        List<Thread> threads = new ArrayList<>();

        int nmb = 0; //какая ячейка вычисляется
        for (int threadNum = 1; threadNum <= n; threadNum++, nmb += numbersPerThread) {
            int finalNmb = nmb;
            Thread thread = new Thread(() -> {
                for (int i = 0; i < numbersPerThread; i++) {
                    int stolbec = (finalNmb + i) % d; //где то здесь ошибка
                    int stroka = (finalNmb + i) / d; //
                    resultMatrix[stroka][stolbec] = internalCounter(firstMatrix, secondMatrix, stroka, stolbec);
                }
            });
            thread.start();
            threads.add(thread);
        }

        //вычисление отстаточныъх чисел
        for (int i = nmb; i < (numbersPerMainThread + nmb); i++) {
            int stroka = i / d;
            int stolbec = i % d;
            resultMatrix[stroka][stolbec] = internalCounter(firstMatrix, secondMatrix, stroka, stolbec);
        }

        for (Thread thread : threads) {
            try {
                thread.join();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        return resultMatrix;
    }

    private static int internalCounter(int[][] firstMatrix,
                                       int[][] secondMatrix,
                                       int a, int d) { // координаты исчиляемые
        int b = firstMatrix[0].length;
        int result = 0;
        for (int k = 0; k < b; k++) {
            result += firstMatrix[a][k] * secondMatrix[k][d];
        }
         return result;
    }


    private static int inputInteger() {
        int n;
        while (true) {
            try {
                n = scanner.nextInt();
                if (n > 0) return n;
                else System.out.println("некорректное значение, попробуйте еще раз");
            } catch (InputMismatchException ex) {
                System.out.println("Try again.  - Incorrect input: an integer is required)");
            }
        }
    }
}

//todo: попробовать решить с ExecutorService
