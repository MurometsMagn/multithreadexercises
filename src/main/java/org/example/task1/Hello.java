package org.example.task1;
//решение от Дмитрия
//вариант если задано больше потоков, чем количество чисел  - не срабатывает


import java.util.Scanner;

public class Hello {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        System.out.println("Введите количество чисел:");
        int m = scanner.nextInt();
        System.out.println("Введите количество потоков:");
        int n = scanner.nextInt();

        int numbersPerThread = m / n;
        int x = 1;

        for (int threadNum = 1; threadNum <= n; threadNum++) {
            int finalI = x;
            Thread thread = new Thread(() -> {
                printThreadFunction(finalI, numbersPerThread);
            });
            thread.start();
            x += numbersPerThread;
        }

        for (int i = n * numbersPerThread+1; i <= m; i++) {
            System.out.println(i);
        }
    }

    public static void printThreadFunction(int start, int count) {
        for (int i = start; i < start + count; i++) {
            System.out.println(i);
        }
    }
}
