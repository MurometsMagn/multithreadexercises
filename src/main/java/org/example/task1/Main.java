package org.example.task1;/* Задание на создание потока.
 Пользователь вводит два целых числа: m и n. Необходимо запустить n потоков,
  каждый их которых выводит на экран какую-то часть из диапазона целых чисел от 1 до m.
  Определить в программе распределение нагрузки на все n потоков так,
  чтобы они были нагружены плюс-минус равномерно.
 */

import java.util.*;

public class Main {
    static Scanner scanner = new Scanner(System.in);
    static int n; //число потоков
    static int m;

    public static void main(String[] args) throws InterruptedException {
        System.out.println("введите целое число m для задания интервала от 0 до m: ");
        m = inputInteger();

        System.out.println("введите количество потоков n: ");
        n = inputInteger();

        int tmp = m; //итератор по задданному интервалу m


        List<Thread> threads = new LinkedList<>();
        for (int i = 0; i < n; i++) {
            int finalI = i;
            Thread thread = new Thread(() -> printFunc(finalI, n, m));
            thread.start();
            threads.add(thread);
        }

//        while (tmp > 0) {
//            for (int i = 1; i <= n && tmp > 0; i++, tmp--) {
//                int finalTmp = tmp; //эту строчку лямбда потребовала
//                int finalI = i;
//                //threads.add(new Thread(()
//                thread(i)
//                -> printFunc(finalTmp, finalI)));
//            }
//        }

        for (Thread thread : threads) {
            thread.join();
        }
    }


    //private static void printFunc(int tmp, int i) {
    private static void printFunc(int nmbThread, int quantThread, int quantNumber) {
        //tmp = m - tmp + 1;
        for (int i = nmbThread; i < quantNumber; i += quantThread) {
            System.out.println("поток: " + nmbThread + ", целое число из заданного интервала: " + (i + 1));
        }

    }

    private static int inputInteger() {
        int n;
        while (true) {
            try {
                n = scanner.nextInt();
                if (n > 0) return n;
                else System.out.println("некорректное значение, попробуйте еще раз");
            } catch (InputMismatchException ex) {
                System.out.println("Try again.  - Incorrect input: an integer is required)");
            }
        }
    }
}
