package org.example.task5_produserConsumer;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Queue;
import java.util.Scanner;

class ProducerRunnable implements Runnable {
    private final Queue<Integer> numbers;
    private final Object collectionFull;

    ProducerRunnable(Queue<Integer> numbers, Object collectionFull) {
        this.numbers = numbers;
        this.collectionFull = collectionFull;
    }

    @Override
    public void run() {
        readFile();
    }

    private void readFile() {
        try {
            File file = new File("thousNumbers.txt");
            Scanner scanner = new Scanner(file);
            while (scanner.hasNextInt()) {
                addToCollection(scanner.nextInt());
            }
        } catch (FileNotFoundException e) {
            System.out.println("File not found");
        }
    }

    private void addToCollection(int data) {
        synchronized (numbers) {
            synchronized (collectionFull) {
                while (numbers.size() > 100) {
                    try {
                        collectionFull.wait();
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }
            numbers.add(data);
            numbers.notify();
        }
        try {
            Thread.sleep(10);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
