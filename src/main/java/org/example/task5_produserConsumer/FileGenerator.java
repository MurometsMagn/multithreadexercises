package org.example.task5_produserConsumer;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Random;

public class FileGenerator {
    public static void main(String[] args) {
        Random random = new Random();
        File file = new File("thousNumbers.txt");
        BufferedWriter writer = null;
        String rnd;

        try {
            writer = new BufferedWriter(new FileWriter(file));
            for (int i = 0; i < 10_000; i++) {
                rnd = random.nextInt(1000) + "";
                writer.write(rnd);
                writer.newLine();
            }
        } catch (IOException e) {
            System.out.println("I/O error");
        } finally {
            try {
                if (writer != null) writer.close(); // <- не забываем!
            } catch (IOException ee) {
                System.out.println("I/O 2 error");
            }
        }
    }
}
