package org.example.task5_produserConsumer;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;

class ConsumerRunnable implements Runnable {
    private final Queue<Integer> numbers;
    private final Object collectionFull;

    ConsumerRunnable(Queue<Integer> numbers, Object collectionFull) {
        this.numbers = numbers;
        this.collectionFull = collectionFull;
    }

    @Override
    public void run() {
        while (true) fibbonacing();
    }

    private void fibbonacing() {
        int nmb;
        synchronized (numbers) {
            while (numbers.isEmpty()) {
                try {
                    numbers.wait();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
            nmb = numbers.poll();
        }
        synchronized (collectionFull) {
            collectionFull.notify();
        }
        int fibbo = countFibboLoop2(nmb);
        System.out.println("fibonaci for nmb=:" + nmb + " is: " + fibbo);
    }

    private int countFibbo(int input) {
        if (input == 0 || input == 1) return 1;
        return countFibbo(input - 1) + countFibbo(input - 2);
    }

    private int countFibboLoop(int input) {
        List<Integer> fibboArr = new LinkedList<>();
        fibboArr.add(0, 1);
        fibboArr.add(1, 1);
        if (input >= 2) {
            for (int i = 2; i <= input; i++) {
                fibboArr.add(i, (fibboArr.get(i - 1) + fibboArr.get(i - 2)));
            }
        }
        return fibboArr.get(input);
    }

    public static int countFibboLoop2(int input) {
        if (input == 1 || input == 2) return 1;
        int minusTwo = 1;
        int tmp;
        int newNmb = 1;
        for (int i = 3; i <= input; i++) {
            tmp = newNmb + minusTwo;
            minusTwo = newNmb;
            newNmb = tmp;
        }
        return newNmb;
    }
}

//переписать на цикл