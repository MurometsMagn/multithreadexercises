package org.example.task5_produserConsumer;
/*
 Предварительно любым способом подготовить текстовый файл, содержащий не менее 10000 целых чисел
 в диапазоне от 1 до 1000 по числу на каждой строке.

В программе реализовать поток-поставщик, читающий этот файл и помещающий прочитанные числа
в общую коллекцию (массив или очередь).
Реализовать задержку в 10 миллисекунд между помещением очередного числа в коллекцию.
Если размер коллекции превышает 100 элементов, поток-поставщик засыпает до тех пор,
пока в коллекции не окажется менее 100 элементов.

Реализовать поток-потребитель и запустить его в четырех экземплярах.
Этот поток считывает числа из общей коллекции, вычисляет числа
Фибоначчи с номером равным прочитанному числу, и выводит их на экран.
Переполнение при вычислении числа Фибоначчи допустимо,
так что отрицательные числа в выводе программы не являются ошибкой.
В случае, если коллекция пуста, поток-потребитель засыпает до тех пор,
пока в коллекции не появятся элементы.

Засыпание потоков реализовать при помощи условных переменных.
В задаче нельзя использовать потокобезопасные (в т.ч. неблокирующие) коллекции.
 */

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;

public class Main {
    //List<Integer> numbers = new ArrayList<>();
    private final Queue<Integer> numbers = new LinkedList<>();
    private final List<Thread> threads = new ArrayList<>();
    private final Object collectionFull = new Object();

    public Main() {
        activating();
    }

    private void activating() {
        Runnable producerRunnable = new ProducerRunnable(numbers, collectionFull);
        Thread produserThread = new Thread(producerRunnable);
        produserThread.start();
        threads.add(produserThread);

        for (int i = 1; i <= 4; i++) {
            Runnable consumerRunnable = new ConsumerRunnable(numbers, collectionFull);
            Thread consumerThread = new Thread(consumerRunnable);
            consumerThread.start();
            threads.add(consumerThread);
        }

        for (Thread thread : threads) {
            try {
                thread.join();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    public static void main(String[] args) {
        new Main();
    }
}
