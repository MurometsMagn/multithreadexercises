package org.example.task6_atomicVars;

import java.util.concurrent.atomic.AtomicInteger;

public class LemerRandom {
    private AtomicInteger a = new AtomicInteger();
    private AtomicInteger c = new AtomicInteger();
    private AtomicInteger m = new AtomicInteger();
    private AtomicInteger x0 = new AtomicInteger(); //предыдыущий

//    public int lemerRandomyzer() {
//        a %= m; //a = a % m
//        c %= m;
//        x0 %= m; //предыдущее значение
//        return (a * x0 + c) % m;
//    }

    private int lemerRandomyzer(int a, int c, int m, int x0) {
        a %= m; //a = a % m
        c %= m;
        x0 %= m; //предыдущее значение
        return (a * x0 + c) % m;
    }
}
