package org.example.task6_atomicVars;

import java.util.ArrayList;
import java.util.InputMismatchException;
import java.util.List;
import java.util.Scanner;

/*
 Пользователь вводит целые положительные числа n (размер массива) и m (количество потоков).
 Программа должна заполнить массив случайными числами, используя m потоков.
 Функцию генерации случайных чисел требуется реализовать без использования
 встроенных средств генерации случайных чисел.
 Вместо этого следует воспользоваться формулой линейного конгруэнтного генератора
 (https://ru.wikipedia.org/wiki/%D0%9B%D0%B8%D0%BD%D0%B5%D0%B9%D0%BD%D1%8B%D0%B9_%D0%BA%D0%BE%D0%BD%D0%B3%D1%80%D1%83%D1%8D%D0%BD%D1%82%D0%BD%D1%8B%D0%B9_%D0%BC%D0%B5%D1%82%D0%BE%D0%B4).
 Функция генерации чисел должна быть многопоточной, то есть позволять одновременный вызов из нескольких потоков.
 Каждые 100 вызовов должны меняться параметры a, c и m.
 Способ смены параметров не имеет значения.
 */

public class Main {
    private final Scanner scanner = new Scanner(System.in);
    private final List<Thread> threads = new ArrayList<>();
    private int arrayLength;
    private int threadsQuantity;


    public Main() {
        init();
    }

    private void init() {
        System.out.println("Введите размер массива: ");
        arrayLength = inputInteger();
        System.out.println("Введите количество потоков: ");
        threadsQuantity = inputInteger();

        int[] randArray = new int[arrayLength];
        for (int i = 0; i < threadsQuantity; i++) {
            Thread thread = new Thread(new Runner());
            thread.start();
            threads.add(thread);
        }
    }

    private int lemerRandomyzer(int a, int c, int m, int x0) {
        a %= m; //a = a % m
        c %= m;
        x0 %= m; //предыдущее значение
        return (a * x0 + c) % m;
    }

    private int inputInteger() {
        int inputData;
        while (true) {
            try {
                inputData = scanner.nextInt();
                if (inputData > 0) return inputData;
                else System.out.println("некорректное значение, попробуйте еще раз");
            } catch (InputMismatchException ex) {
                System.out.println("Try again.  - Incorrect input: an integer is required)");
            }
        }
    }

    public static void main(String[] args) {
        new Main();
    }
}
