package org.example.task9_suspendKotlin;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class Enemy {
    int fieldWidth;
    int fieldHeight;
    List <Point> root = new ArrayList<>();
    Random random = new Random();

    public Enemy(int fieldWidth, int fieldHeight) {
        this.fieldWidth = fieldWidth;
        this.fieldHeight = fieldHeight;
        root = generateRoot();
    }

    private List<Point> generateRoot() {
        List<Point> root = new ArrayList<>();
        Point startPoint = new Point(random.nextInt(fieldHeight + 1), random.nextInt(fieldWidth + 1));
        root.add(startPoint);
        Point previousPoint = startPoint;
        Point nextPoint;
        loop:
        do {
            int step = random.nextInt(4 + 1); // 1-up, 2-right, 3-down, 4-left
            switch (step) {
                case 1 -> nextPoint = new Point(previousPoint.row + 1, previousPoint.column);
                case 2 -> nextPoint = new Point(previousPoint.row, previousPoint.column + 1);
                case 3 -> nextPoint = new Point(previousPoint.row - 1, previousPoint.column);
                case 4 -> nextPoint = new Point(previousPoint.row, previousPoint.column - 1);
                default -> {
                    System.out.println("unexpected value..");
                    nextPoint = new Point(); //заглушка
                }
            };
            if (nextPoint.row >= 0 && nextPoint.row <= fieldWidth &&
                    nextPoint.column >= 0 && nextPoint.column <= fieldHeight) {
                root.add(nextPoint);
                previousPoint = nextPoint;
            }
        } while (nextPoint.row != startPoint.row && nextPoint.column != nextPoint.column);

        return root;
    }

}
