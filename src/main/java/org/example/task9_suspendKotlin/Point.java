package org.example.task9_suspendKotlin;

public class Point {
    public int row;
    public int column;

    public Point(int row, int column) {
        this.row = row;
        this.column = column;
    }

    public Point() {
        row = 1;
        column = 1;
    }
}
