package org.example.task9_suspendKotlin.tryKotlin

import org.example.task9_suspendKotlin.async_without_async.Character
import kotlin.Throws
import org.example.task9_suspendKotlin.async_without_async.CollisionException
import org.example.task9_suspendKotlin.async_without_async.Field

class Enemy(field: Field?, row: Int, col: Int) : Character(field, row, col) {
    private var state = 0
    private var stepsDone = 0
    override fun getChar(): Char {
        return '*'
    }

    @Throws(CollisionException::class)
    override fun move() { //асинхронность
        when (state) {
            0 -> goLeft()
            1 -> goDown()
            2 -> goRight()
            3 -> goUp()
        }
        if (++stepsDone == 3) {
            stepsDone = 0
            state = (state + 1) % 4
        }
        sleep()
    }
}

/*

    override suspend fun move() {
        int stepsDone = 0;
        int direction = 0;
        while (true) {
            switch (state) {
                case 0:
                    goLeft();  // await
                    break;
                case 1:
                    goDown();  // await
                    break;
                case 2:
                    goRight();  // await
                    break;
                case 3:
                    goUp();  // await
                    break;
            }
            if (++stepsDone == 3) {
                stepsDone = 0;
                state = (state + 1) % 4;
            }
            yield();  // await
        }
    }
     */
