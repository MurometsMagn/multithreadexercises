package org.example.task9_suspendKotlin.tryKotlin

data class Point(var row: Int, var column: Int) {

    constructor() : this(1, 1)
}