package org.example.task9_suspendKotlin.async_without_async;

public class OutOfFieldException extends Exception {
    public OutOfFieldException(String message) {
        super(message);
    }
}
