package org.example.task9_suspendKotlin.async_without_async;

import java.util.Arrays;
import java.util.stream.Collectors;

public class Field {
    private final int width;
    private final int height;
    private final char[][] map;

    public Field(int width, int height) {
        this.width = width;
        this.height = height;
        map = new char[width][height];
        for (int i = 0; i < width; i++) {
            for (int j = 0; j < height; j++) {
                map[i][j] = ' ';
            }
        }
    }

    public int getWidth() {
        return width;
    }

    public int getHeight() {
        return height;
    }

    public char getCellContents(int row, int col) {
        return map[row][col];
    }

    public void putChar(int row, int col, char ch) throws OutOfFieldException {
        checkCoords(row, col);
        map[row][col] = ch;
    }

    public void moveCell(int fromRow, int fromCol, int toRow, int toCol) throws OutOfFieldException, CollisionException {
        checkCoords(toRow, toCol);
        char ch = map[fromRow][fromCol];
        checkCollision(ch, toRow, toCol);
        map[fromRow][fromCol] = ' ';
        map[toRow][toCol] = ch;
    }

    private void checkCoords(int row, int col) throws OutOfFieldException {
        if (row < 0 || row >= height || col < 0 || col >= width) {
            throw new OutOfFieldException("Cell is out of field");
        }
    }

    private void checkCollision(char whoMoved, int row, int col) throws CollisionException {
        if (map[row][col] != ' ') throw new CollisionException(whoMoved, map[row][col]);
    }

    @Override
    public String toString() {
        return Arrays.stream(map).map(String::valueOf).collect(Collectors.joining("\n"));
    }
}
