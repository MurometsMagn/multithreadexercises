package org.example.task9_suspendKotlin.async_without_async;

import java.util.Random;
import java.util.Scanner;

public class Game {
    private static final int ENEMY_COUNT = 2;

    private static final Scanner scanner = new Scanner(System.in);
    private static final Random random = new Random();

    public static void main(String[] args) {
        Field field = createField();
        Character [] characters = new Character[ENEMY_COUNT + 1];

        Player player;

        try {
            characters[0] = player = createPlayer(field, generateCharacterPosition(field));
            for (int i = 1; i <= ENEMY_COUNT; i++) {
                characters[i] = createEnemy(field, generateCharacterPosition(field));
            }
        } catch (OutOfFieldException e) {
            System.out.println("Ошибка работы программы");
            return;
        }

        while (true) {
            try {
                for (Character character : characters) {
                    character.move();
                }

                if (player.getRow() == 0) {
                    System.out.println("Вы победили");
                    break;
                }

                System.out.println(field);
                System.out.println("===");
            } catch (CollisionException e) {
                if (e.getWhoMoved() == 'X' || e.getWhoStayed() == 'X') {
                    System.out.println("Вы проиграли");
                    break;
                }
            }
        }

        System.out.println(field);
    }

    private static Field createField() {
        System.out.print("Введите ширину поля: ");
        int width = scanner.nextInt();
        System.out.print("Введите высоту поля: ");
        int height = scanner.nextInt();

        return new Field(width, height);
    }

    private static Point generateCharacterPosition(Field field) {
        return new Point(random.nextInt(field.getHeight()), random.nextInt(field.getWidth()));
    }

    private static Player createPlayer(Field field, Point pos) throws OutOfFieldException {
        return new Player(field, pos.row, pos.col);
    }

    private static Character createEnemy(Field field, Point pos) throws OutOfFieldException {
        return new Enemy(field, pos.row, pos.col);
    }

    private static class Point {
        int row;
        int col;

        public Point(int row, int col) {
            this.row = row;
            this.col = col;
        }
    }
}
