package org.example.task9_suspendKotlin.async_without_async;

public abstract class Character {
    protected final Field field;
    protected int posRow;
    protected int posCol;

    public Character(Field field, int row, int col) throws OutOfFieldException {
        this.field = field;
        this.posRow = row;
        this.posCol = col;
        field.putChar(row, col, getChar());
    }

    protected abstract char getChar();
    public abstract void move() throws CollisionException;

    public int getRow() {
        return posRow;
    }

    public int getCol() {
        return posCol;
    }

    public void goLeft() throws CollisionException {
        try {
            field.moveCell(posRow, posCol, posRow, posCol - 1);
            posCol--;
        } catch (OutOfFieldException e) {
            //
        }
    }

    public void goRight() throws CollisionException {
        try {
            field.moveCell(posRow, posCol, posRow, posCol + 1);
            posCol++;
        } catch (OutOfFieldException e) {
            //
        }
    }

    public void goUp() throws CollisionException {
        try {
            field.moveCell(posRow, posCol, posRow - 1, posCol);
            posRow--;
        } catch (OutOfFieldException e) {
            //
        }
    }

    public void goDown() throws CollisionException {
        try {
            field.moveCell(posRow, posCol, posRow + 1, posCol);
            posRow++;
        } catch (OutOfFieldException e) {
            //
        }
    }

    protected void sleep() {
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
