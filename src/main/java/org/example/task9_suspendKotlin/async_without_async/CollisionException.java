package org.example.task9_suspendKotlin.async_without_async;

public class CollisionException extends Exception {
    private final char whoMoved;
    private final char whoStayed;

    public CollisionException(char whoMoved, char whoStayed) {
        this.whoMoved = whoMoved;
        this.whoStayed = whoStayed;
    }

    public char getWhoMoved() {
        return whoMoved;
    }

    public char getWhoStayed() {
        return whoStayed;
    }
}
