package org.example.task9_suspendKotlin.async_without_async;

public class Player extends Character {
    public Player(Field field, int row, int col) throws OutOfFieldException {
        super(field, row, col);
    }

    @Override
    protected char getChar() {
        return 'X';
    }

    @Override
    public void move() throws CollisionException {
        goUp();
        sleep();
    }
}
