package org.example.task9_suspendKotlin.async_without_async;

public class Enemy extends Character {
    private int state = 0;
    private int stepsDone = 0;

    public Enemy(Field field, int row, int col) throws OutOfFieldException {
        super(field, row, col);
    }

    @Override
    protected char getChar() {
        return '*';
    }

    @Override
    public void move() throws CollisionException { //асинхронность
        switch (state) {
            case 0:
                goLeft();
                break;
            case 1:
                goDown();
                break;
            case 2:
                goRight();
                break;
            case 3:
                goUp();
                break;
        }
        if (++stepsDone == 3) {
            stepsDone = 0;
            state = (state + 1) % 4;
        }

        sleep();
    }

    /*
    override suspend fun move() {
        int stepsDone = 0;
        int direction = 0;
        while (true) {
            switch (state) {
                case 0:
                    goLeft();  // await
                    break;
                case 1:
                    goDown();  // await
                    break;
                case 2:
                    goRight();  // await
                    break;
                case 3:
                    goUp();  // await
                    break;
            }
            if (++stepsDone == 3) {
                stepsDone = 0;
                state = (state + 1) % 4;
            }
            yield();  // await
        }
    }
     */
}
