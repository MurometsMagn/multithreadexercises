package org.example.task9_suspendKotlin.fromDmitry
import kotlinx.coroutines.delay
import kotlinx.coroutines.yield
import java.util.*
import kotlin.coroutines.Continuation
import kotlin.coroutines.resume
import kotlin.coroutines.suspendCoroutine
import kotlin.random.Random

class Teacher {
    private var running = false
    private val queue: Queue<StudentInfo> = LinkedList()

    suspend fun consult(studentNum: Int) = suspendCoroutine<Unit> { continuation ->
        queue.offer(StudentInfo(studentNum, continuation))
    }

    suspend fun teach() {
        running = true
        while (running) {
            val info = queue.poll()
            if (info != null) {
                doConsult(info)
            } else {
                yield()
            }
        }
    }

    fun stop() {
        running = false
    }

    private suspend fun doConsult(info: StudentInfo) {
        println("Consulting student number ${info.studentNum}")
        delay(Random.nextLong(100, 1000))
        println("Done consulting student number ${info.studentNum}")
        info.continuation.resume(Unit)
    }

    private data class StudentInfo(val studentNum: Int, val continuation: Continuation<Unit>)
}