package org.example.task9_suspendKotlin.fromDmitry

import kotlinx.coroutines.joinAll
import kotlinx.coroutines.launch
import kotlinx.coroutines.runBlocking

fun main(args: Array<String>) {
    val teacher = Teacher()

    runBlocking {
        launch { teacher.teach() }
        val tasks = (1..5).map { num -> launch { Student(num, teacher).study() } }
        tasks.joinAll()
        teacher.stop()
    }
}