package org.example.task9_suspendKotlin.fromDmitry

import kotlinx.coroutines.delay
import kotlin.random.Random

class Student(private val num: Int, private val teacher: Teacher) {
    suspend fun study() {
        repeat(10) {
            think()
            consult()
        }
    }

    private suspend fun think() {
        println("Student number $num is thinking")
        delay(Random.nextLong(500, 1500))
    }

    private suspend fun consult() {
        println("Student number $num is waiting for teacher")
        teacher.consult(num)
    }
}