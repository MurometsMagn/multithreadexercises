package org.example.task3_simpleTreadSinc;

/*
Пользователь вводит целое положительное число m -- размер массива,
 -- а также целое положительное число n -- количество потоков.
 Содержимое массива генерируется случайным образом.
 Необходимо найти сумму элементов массива двумя способами: однопоточно и многопоточно.
 Время решения необходимо замерить, отобразить и сделать выводы.
 При решении воспользоваться мьютексами или мониторами в зависимости от предпочтений.


Примечание: так как в критической секции может находиться ровно один поток,
чтобы не превращать многопоточное решение в последовательное,
следует минимизировать количество кода, заключенного в критическую секцию.
 */

import java.time.Duration;
import java.time.Instant;
import java.util.*;

public class SimpleThreadSincronize {
    private static final Scanner scanner = new Scanner(System.in);
    static int result = 0;

    public static void main(String[] args) throws InterruptedException {
        System.out.println("Введите размер массива m:");
        int m = inputInteger();

        System.out.println("Введите количество потоков n:");
        int n = inputInteger();

        int[] matrix = initMatrixRandom(m);
        soutMatrix(matrix);

        Instant start = Instant.now();
        singleThreadCount(matrix);
        Instant finish = Instant.now();
        long elapsed = Duration.between(start, finish).toMillis();
        System.out.println("Однопоточное вычисление, мс: " + elapsed);
        System.out.println("result = " + result);

        long start2 = System.currentTimeMillis(); //либо System.nanoTime();
        multyThreadCount(matrix, n);
        long finish2 = System.currentTimeMillis();
        long elapsed2 = finish2 - start2;
        System.out.println("Многопоточное вычисление, мс: " + elapsed2);
        System.out.println("result2 = " + SimpleThreadSincronize.result);
    }

    private static void singleThreadCount(int[] matrix) {
        result = 0;
        for (int j : matrix) {
            result += j;
        }
    }

    private static void multyThreadCount(int[] matrix, int threadsQuantity) throws InterruptedException {
        result = 0;
        int arrQuantity = matrix.length;
        if (threadsQuantity > arrQuantity) threadsQuantity = arrQuantity;
        int numbersPerThread = arrQuantity / threadsQuantity;
        int numbersPerMainThread = arrQuantity % threadsQuantity;
        int number = 0; //какая ячейка вычисляется
        //Lock lock = new ReentrantLock();
        Object lockObject = new Object(); //чтобы по нему синхронизировать
        List<Thread> threads = new ArrayList<>();

        for (int i = 0; i < threadsQuantity; i++) {
            Runnable runnable = new MyRunnable(numbersPerThread, matrix, lockObject, number);
            Thread thread = new Thread(runnable);
            thread.start();
            threads.add(thread);
            number += numbersPerThread;
        }

        Runnable runnable = new MyRunnable(numbersPerMainThread, matrix, lockObject, number);
        runnable.run();

        for (Thread thread : threads) {
            try {
                thread.join();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    private static int[] initMatrixRandom(int x) {
        int[] matrix = new int[x];
        for (int j = 0; j < x; j++) {
            matrix[j] = (int) (Math.random() * 10);
        }
        return matrix;
    }

    private static void soutMatrix(int[] matrix) {
//        for (int i = 0; i < matrix.length; i++) {
//            System.out.println(matrix[i]);
//        }
        for (int j : matrix) {
            System.out.print(j + "  ");
        }
        System.out.println();
    }

    private static int inputInteger() {
        int n;
        while (true) {
            try {
                n = scanner.nextInt();
                if (n > 0) return n;
                else System.out.println("некорректное значение, попробуйте еще раз");
            } catch (InputMismatchException ex) {
                System.out.println("Try again.  - Incorrect input: an integer is required)");
            }
        }
    }

    private static class MyRunnable implements Runnable {
        private final int numbersPerThread;
        private final int[] matrix;
        private final Object lockObject;
        private final int number;

        public MyRunnable(int numbersPerThread, int[] matrix, Object lockObject, int number) {
            this.numbersPerThread = numbersPerThread;
            this.matrix = matrix;
            this.lockObject = lockObject;
            this.number = number;
        }

        @Override
        public void run() {
            int localSum = 0;

            for (int i = 0; i < numbersPerThread; i++) {
                localSum += matrix[number + i];
            }
            synchronized (lockObject) {
                SimpleThreadSincronize.result += localSum;
            }
        }
    }
}
