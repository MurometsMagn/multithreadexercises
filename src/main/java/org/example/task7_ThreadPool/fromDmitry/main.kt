package org.example.task7_ThreadPool.fromDmitry

fun <T> ThreadPool<T>.withPool(block: (ThreadPool<T>) -> Unit) {
    start()
    try {
        block(this)
    } finally {
        askStop()
        joinAll()
    }
}

fun main(args: Array<String>) {
    ThreadPool<Int>(4) { println(it * it) }
            .withPool { pool ->
                for (i in 1..1000) {
                    pool.addTask(i)
                    Thread.sleep(10)
                }
            }
}