package org.example.task7_ThreadPool.fromDmitry

import java.util.concurrent.ConcurrentLinkedQueue

class ThreadPool<T>(numThreads: Int, private val handleTask: (T) -> Unit) {
    private val taskQueue = ConcurrentLinkedQueue<T>()
    private val workerThreads = Array(numThreads) {
        Thread(this::runThread)
    }
    @Volatile
    private var running = false

    fun addTask(task: T) {
        taskQueue.offer(task)
    }

    fun start() {
        running = true
        for (thread in workerThreads) {
            thread.start()
        }
    }

    fun joinAll() {
        for (thread in workerThreads) {
            thread.join()
        }
    }

    fun askStop() {
        running = false
    }

    private fun runThread() {
        while (running) {
            val task = taskQueue.poll()
            if (task != null) {
                handleTask(task)
            }
            Thread.sleep(1)
        }
    }
}