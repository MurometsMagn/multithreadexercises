package org.example.task7_ThreadPool;

import org.example.task8_paralDataStructures.Consumer;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;
import java.util.concurrent.ExecutorService;

public class Main {
    public static void main(String[] args) {

        MyThreadPool<Integer> myThreadPool = new MyThreadPool<>(4, Main::calculateFibbo);
        myThreadPool.start(); // запуск потоков
        readFile(myThreadPool);
        myThreadPool.askStop();
        myThreadPool.joinAll();
    }

    private static void readFile(MyThreadPool<Integer> myThreadPool) {
        try {
            File file = new File("thousNumbers.txt");
            Scanner scanner = new Scanner(file);
            while (scanner.hasNextInt()) {
                myThreadPool.addTask(scanner.nextInt());
            }
        } catch (FileNotFoundException e) {
            System.out.println("File not found");
        }
    }

    private static void calculateFibbo(int input) {
        int minusTwo = 1;
        int tmp;
        int newNmb = 1;
        for (int i = 3; i <= input; i++) {
            tmp = newNmb + minusTwo;
            minusTwo = newNmb;
            newNmb = tmp;
        }
        System.out.println(newNmb);
    }
}

//todo: в задаче на матрицы использовать MyThreadPool (сам MyThreadPool изменять нельзя, попробовать импортировать из др. пекеджа
