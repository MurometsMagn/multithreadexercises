package org.example.task7_ThreadPool;

import java.util.ArrayList;
import java.util.List;
import java.util.Queue;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.function.Consumer;

public class MyThreadPool<T> {
    private final Queue<T> taskQueue = new ConcurrentLinkedQueue<T>();
    private final List<Thread> actionThreads = new ArrayList<>();
    private volatile boolean isRunning = false;
    private Consumer<T> action;

    public MyThreadPool(int nThreads, Consumer<T> action) {
        this.action = action;
        for (int i = 0; i < nThreads; i++) {
            actionThreads.add(new Thread(this::runThread)); //передача функции как параметра
        }
    }

    public void addTask(T task) {
        taskQueue.offer(task);
    }

    public void start() {
        isRunning = true;
        for (Thread thread : actionThreads) {
            thread.start();
        }
    }

    public void joinAll() {
        for (Thread thread : actionThreads) {
            try {
                thread.join();
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    public void askStop() {
        isRunning = false;
    }


    private void runThread() {
        while (isRunning || !taskQueue.isEmpty()) {
            var task = taskQueue.poll();
            if (task != null) {
                action.accept(task);
            }
            try {
                Thread.sleep(1);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}
