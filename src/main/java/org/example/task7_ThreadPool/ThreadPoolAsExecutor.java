package org.example.task7_ThreadPool;

import java.util.Queue;
import java.util.concurrent.*;

/*
 Реализовать класс ThreadPool, который отвечает за запуск заданного количества потоков и управление ими.
 Его конструктор принимает количество потоков, которые необходимо запустить,
 а также функцию (команду, передаваемую средствами вашего языка программирования),
 которая предназначена для обработки элементов.
 Элементы передаются в потоки при помощи потокобезопасной очереди,
 хранящейся в виде поля класса ThreadPool.

Пример использования созданного класса ThreadPool на Java:

ThreadPool<Integer> pool = new ThreadPool<>(4, (number) -> {  // 4 потока, печатающих числа на экран
  System.out.println(number);
});
pool.start(); // запуск потоков
pool.push(42); // отправить значение 42 на обработку одним из потоков

Реализовать задачу на подход поставщик-потребитель, с использованием созданного класса ThreadPool.
 */
public class ThreadPoolAsExecutor implements Executor {
    private final Queue<Runnable> workQueue = new ConcurrentLinkedQueue<>();
    private volatile boolean isRunning = true;

    public ThreadPoolAsExecutor(int nThreads) {
        for (int i = 0; i < nThreads; i++) {
            new Thread(new TaskWorker()).start();
        }
    }


    @Override
    public void execute(Runnable command) {
        if (isRunning) {
            workQueue.offer(command);
        }
    }

    public void shutdown() {
        isRunning = false;
    }

    private final class TaskWorker implements Runnable {

        @Override
        public void run() {
            while (isRunning) {
                Runnable nextTask = workQueue.poll();
                if (nextTask != null) {
                    nextTask.run();
                }
            }
        }
    }
}

//ThreadPool - это всегда consumer