package org.example.task8_paralDataStructures;

public class Consumer implements Runnable{
    private final int primalNumber;

    public Consumer(int primalNumber) {
        this.primalNumber = primalNumber;
    }

    @Override
    public void run() {
        System.out.println(countFibbo(primalNumber));
    }

    public static int countFibbo(int input) {
        if (input == 1 || input == 2) return 1;
        int minusTwo = 1;
        int tmp;
        int newNmb = 1;
        for (int i = 3; i <= input; i++) {
            tmp = newNmb + minusTwo;
            minusTwo = newNmb;
            newNmb = tmp;
        }
        return newNmb;
    }
}
