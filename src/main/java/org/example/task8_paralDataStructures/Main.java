package org.example.task8_paralDataStructures;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.LinkedList;
import java.util.Queue;
import java.util.Scanner;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

//В данном случае Поставщик - это сам главный поток
public class Main {
    public static void main(String[] args) {
        int nThreads = 4; //запрашивать?
        ExecutorService executorService = Executors.newFixedThreadPool(nThreads);
        final Queue<Integer> numbers = new ConcurrentLinkedQueue<>();

        readFile(executorService);

        executorService.shutdown();
    }

    //producer
    private static void readFile(ExecutorService executorService) {
        try {
            File file = new File("thousNumbers.txt");
            Scanner scanner = new Scanner(file);
            while (scanner.hasNextInt()) {
                //addToCollection(scanner.nextInt());
                executorService.submit(new Consumer(scanner.nextInt()));
            }
        } catch (FileNotFoundException e) {
            System.out.println("File not found");
        }
    }
}
