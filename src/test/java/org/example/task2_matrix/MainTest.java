package org.example.task2_matrix;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertArrayEquals;

class MainTest {
    int a = 2;
    int b = 2;
    int c = 2;
    int d = 3;

    int[][] firstMatrix = {{1, 2}, {3, 4}};
    int[][] secondMatrix = {{1, 2, 3}, {4, 5, 6}};
    int[][] resultMatrix = {{9, 12, 15}, {19, 26, 33}};

    @Test
    public void testSingleThreaded() {
        //TmpForTests tmpForTests = new TmpForTests();
        assertArrayEquals(resultMatrix, Main.singleThreaded(firstMatrix, secondMatrix));
    }


}